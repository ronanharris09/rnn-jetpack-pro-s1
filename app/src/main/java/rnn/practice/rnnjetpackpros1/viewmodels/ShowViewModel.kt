package rnn.practice.rnnjetpackpros1.viewmodels

import androidx.lifecycle.ViewModel
import rnn.practice.rnnjetpackpros1.application.Core
import rnn.practice.rnnjetpackpros1.models.Show

class ShowViewModel : ViewModel() {
    fun getData() : ArrayList<Show> = Core().rawShowsData
    fun getShows(int: Int) : ArrayList<Show> = getData().filter { it.showType == int } as ArrayList<Show>
}