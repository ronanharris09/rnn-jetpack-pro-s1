package rnn.practice.rnnjetpackpros1

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import rnn.practice.rnnjetpackpros1.databinding.ActivityDetailsBinding
import rnn.practice.rnnjetpackpros1.viewmodels.ShowViewModel

class DetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailsBinding
    private val viewModel: ShowViewModel by lazy {
        ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(ShowViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()

        with(viewModel.getShows(intent.getIntExtra("currentShowType", 0))[intent.getIntExtra("currentShowIndex", 0)]) {
            binding.apply {
                Glide.with(this@DetailsActivity)
                    .load(this@with.imgRes)
                    .into(IVPoster)

                TVTitle.text = this@with.showName.toString()
                TVYear.text = this@with.releaseYear.toString()
                TVGenre.text = this@with.showGenre.toString()
                TVContentRating.text = this@with.contentRating.toString()
                TVDuration.text = resources.getString(R.string.TVDuration, this@with.durationHours, this@with.durationMinutes)
                TVType.text = resources.getQuantityText(R.plurals.TVType, this@with.showType ?: 0)
                TVOverview.text = this@with.showOverview.toString()
                BSource.setOnClickListener {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW, Uri.parse(this@with.sourceLink.toString())
                        )
                    )
                }
                FABFavorite.setOnClickListener {
                    Snackbar.make(root, resources.getString(R.string.SnackbarUnavailableFeature), Snackbar.LENGTH_LONG).show()
                    it.isSelected = !it.isSelected
                }
            }

            supportActionBar?.title = resources.getString(R.string.ABDetailsActivity, this.showName)
        }
    }
}